**INSTALLATION AND SET UP**

1. Clone the repo
2. Open 2 terminal windows (one for frontend & one for backend)
3. In the backend terminal follow these steps :
    - cd backend
    - virtualenv -p python3 venv [creates a virtual env]
    - .\venv\Scripts\activate
    - pip install flask
    - pip install flask_restful
    - pip install pandas
    - pip install flask_cors
    - pip install flask_sqlalchemy
    - python api.py [flask backend runs on port 5000]

4. In the frontend terminal follow these steps:

    - cd frontend
    - npm install [download all required node modules]
    - npm start [react frontend runs on port 3000]
