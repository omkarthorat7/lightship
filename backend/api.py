from flask import Flask, render_template, jsonify, request, json
from flask_restful import Resource, Api, reqparse
import pandas as pd
from flask_cors import CORS, cross_origin
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///users.db"


db = SQLAlchemy(app)



#database consisting of the details of the user
#many to one relationship between user and company is yet to be added
#many - user and one - company
class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.Text, nullable=False)
	comp_id = db.Column(db.Integer, db.ForeignKey('company.id'))
	email = db.Column(db.Text, nullable=False)
	contact_number = db.Column(db.Integer, nullable=False)

	def __str__(self):
		return f'{self.id} {self.comp_id} {self.name} {self.email} {self.contact_number}'

def user_serializer(user):
    return {
        'id' : user.id,
        'name' : user.name,
        'comp_id' : user.comp_id,
        'email' : user.email,
        'contact_number' : user.contact_number
    }


#database consisting of the details of the company
class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    scan_name = db.Column(db.Text, nullable=False)
    scan_description = db.Column(db.Text, nullable=False)
    scan_tag = db.Column(db.Text, nullable=False)
    company_name = db.Column(db.Text, nullable=False)
    domain_name = db.Column(db.Text, nullable=False)
    API_endpoints = db.Column(db.Text, nullable=False)
    WebApp = db.Column(db.Boolean, nullable=False)
    DBServer = db.Column(db.Boolean, nullable=False)
    NetworkServer = db.Column(db.Boolean, nullable=False)
    username1 = db.Column(db.Text, nullable=False)
    username2 = db.Column(db.Text, nullable=False)
    password1 = db.Column(db.Text, nullable=False)
    password2 = db.Column(db.Text, nullable=False)
    date = db.Column(db.Date, nullable=False)
    environments = db.relationship('Environment', backref='company', lazy=True)
    users = db.relationship('User', backref='company', lazy=True)

    def __str__(self):
        return f'{self.id} {self.scan_name} {self.scan_description} {self.scan_tag} {self.company_name} {self.domain_name} {self.API_endpoints} {self.WebApp} {self.DBServer} {self.NetworkServer} {self.username1} {self.username2} {self.password1} {self.password2} {self.date}'

def company_serializer(company):
    return {
        'id' : company.id,
        'scan_name' : company.scan_name,
        'scan_description' : company.scan_description,
        'scan_tag' : company.scan_tag,
        'company_name' : company.company_name,
        'domain_name' : company.domain_name,
        'API_endpoints' : company.API_endpoints,
        'WebApp' : company.WebApp,
        'DBServer' : company.DBServer,
        'NetworkServer' : company.NetworkServer,
        'username1' : company.username1,
        'username2' : company.username2,
        'password1' : company.password1,
        'password2' : company.password2,
        'date' : company.date
    }

#database consisting of the details of the environments - It is the many side in the One to Many relationship
#between the user(one) and environment(many)
class Environment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))
    env_name = db.Column(db.Text, nullable=False)
    url = db.Column(db.Text, nullable=False)
    env_no_subdomains = db.Column(db.Integer, nullable=False)
    subdomains = db.relationship('Subdomain', backref='environment', lazy=True)

    def __str__(self):
        return f'{self.id} {self.company_id} {self.env_name} {self.url}  {self.env_no_subdomains} '


def env_serializer(env):
    return {
        'id' : env.id,
        'company_id' : env.company_id,
        'env_name' : env.env_name,
        'url' : env.url,
        'env_no_subdomains' : env.env_no_subdomains,
    }

#database consisting of the details of the environments - It is the many side in the One to Many relationship
#between the user(one) and environment(many)
class Subdomain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subname = db.Column(db.Text, nullable=False)
    inscope = db.Column(db.Boolean, nullable=False)
    env_id = db.Column(db.Integer, db.ForeignKey('environment.id'))
    

    def __str__(self):
        return f'{self.id} {self.env_id} {self.subname} {self.inscope}'


def subdomain_serializer(sub):
    return {
        'id' : sub.id,
        'subname' : sub.env_name,
        'inscope' : sub.inscope,
        'env_id' : sub.env_id,
    }



api = Api(app)
    
#returns list of all the companies
@app.route('/company', methods = ['GET'])
@cross_origin()
def get_company() :
    return jsonify([*map(company_serializer, Company.query.all())])

#function to add company to the database
@app.route('/company/create', methods = ['POST'])
@cross_origin()
def create_company():
    request_data = json.loads(request.data)
    print("This is the state : \n")
    print(request_data['responsedata'])
    #creating the company
    newcompany = Company(scan_name =  request_data['responsedata']['scan_name'], 
        scan_description =  request_data['responsedata']['scan_description'], 
        scan_tag =  request_data['responsedata']['scan_tag'], 
        company_name =  request_data['responsedata']['company_name'], 
        domain_name = request_data['responsedata']['domain_name'],
        API_endpoints = request_data['responsedata']['API_endpoints'],
        WebApp = request_data['responsedata']['WebApp'],
        DBServer = request_data['responsedata']['DBServer'],
        NetworkServer = request_data['responsedata']['NetworkServer'],
        username1 = request_data['responsedata']['username1'],
        username2 = request_data['responsedata']['username2'],
        password1 = request_data['responsedata']['password1'],
        password2 = request_data['responsedata']['password2'],
        date = datetime.strptime(str(request_data['responsedata']['date']), '%Y-%m-%d' ).date())
    print(newcompany)
    #committing the company to database
    db.session.add(newcompany)
    db.session.commit()
    #getting the company id so that it can be used as foreign key
    company_ID = Company.query.filter_by(company_name=request_data['responsedata']['company_name']).first().id
    print('company id is ' + str(company_ID))
    #creating the user
    newuser = User(name = request_data['responsedata']['name'] ,
    	email =  request_data['responsedata']['email'],
    	comp_id = company_ID,
    	contact_number =request_data['responsedata']['contact_no'] )
    print(newuser)
    db.session.add(newuser)
    db.session.commit()
    #creating the environment
    i = 0;
    envcount = 0;
    if(len(request_data['responsedata']['envNum']) == 0):
    	envcount = 0
    else :
    	envcount = int(request_data['responsedata']['envNum']);
    print('env count is'+ str(envcount));
    #pushing all the environments corresponding to a company
    while(i < envcount):
    	newEnv = Environment(env_name = request_data['responsedata']['envNames'][i],
    		company_id = company_ID,
    		url = request_data['responsedata']['envUrls'][i],
    		env_no_subdomains = request_data['responsedata']['envSubdomains'][i])
    	db.session.add(newEnv);
    	db.session.commit();
    	print(newEnv)
    	envId = Environment.query.filter_by(env_name=request_data['responsedata']['envNames'][i]).first().id
    	subdomaincount = int(request_data['responsedata']['envSubdomains'][i])
    	j = 0;
    	while(j < subdomaincount):
    		newsubdomain = Subdomain(subname = request_data['responsedata']['subdomainName'][i][str(j)],
    		inscope= request_data['responsedata']['InScope'][i][str(j)],
    		env_id = envId)
    		db.session.add(newsubdomain)
    		db.session.commit()
    		j = j + 1;
    	i = i + 1;
    

    response = jsonify('Hello');
    response.headers.add('Access-Control-Allow-Origin', 'http:localhost')
    return response;

if __name__ == '__main__':
    app.run(debug=True)  # run our Flask app
