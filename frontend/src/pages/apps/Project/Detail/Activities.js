import React from 'react';
import { Media } from 'reactstrap';
import classNames from 'classnames';
import { Loader } from 'react-feather';

const Entry = (props) => {
    return <Media>
        <div className="media-body overflow-hidden">
            <h5 className="font-size-15 mt-2 mb-1">{props.user}</h5>
            <p className="text-muted font-size-13 text-truncate mb-0">{props.description}</p>
        </div>
    </Media>
}


const Details = (props) => {
    const web = props.isWeb;
    let TechnicalData = [];
    if(props.isWeb === true){
        TechnicalData = [
        {user : "Data", description : "Neque porro quisquam est"},
        {user : "Request", description : "Neque porro quisquam est"},
        {user : "Response", description : "Neque porro quisquam est"},
        {user : "Method", description : "Neque porro quisquam est"},
        {user : "Param Name", description : "Neque porro quisquam est"},
        {user : "Params", description : "Neque porro quisquam est"},
        {user : "Path", description : "Neque porro quisquam est"},
        {user : "Status Code", description : "Neque porro quisquam est"},
        {user : "Query", description : "Neque porro quisquam est"},
        {user : "Website", description : "Neque porro quisquam est"}
        ];
    }
    else{
        TechnicalData = [{user : "Data", description : "Neque porro quisquam est"}]
    }
    return (
        <React.Fragment>
            <h6 className="mt-0 header-title">TECHNICAL DETAILS</h6>

            <ul className="list-unstyled activity-widget">
                {TechnicalData.map((object, index) => {
                        return (
                        <li key={`${object}_${index}`}>
                            <Entry user={object.user} description={object.description} classNames="bg-soft-primary text-primary" />
                        </li>
                        )
                })}
            </ul>

        </React.Fragment>
    );
};

export default Details;
