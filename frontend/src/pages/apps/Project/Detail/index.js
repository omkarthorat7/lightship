import React, { Component } from 'react';
import { Row, Col, Card, CardBody, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import classNames from 'classnames';

import ProjectStats from './Stats';
import About from './About';
import Comments from './Comments';
import Attachments from './Attachments';
import Details from './Activities';


class ProjectDetail extends Component {

    constructor(props) {
        super(props)

        this.state = {
            project: {
                title: 'Vulnerability Name',
                criticality : 'HIGH',
                easeOfResolution : 'MODERATE',
                isWeb : true,
                shortDesc: 'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
                state: 'Completed',
                owner: 'Rick Perry'
            },
            myprojectcriticality : '',
            activeTab: 'comments'
        }

        this.toggleTab = this.toggleTab.bind(this);
    }
    componentDidMount(){
        let index = 0;
        fetch('http://127.0.0.1:4000/vulnerability',{
          method : 'GET'})
        .then(response =>{
            if(response.ok){
              return (response.json())       
            }
            else{
              console.log('some error in getting the list'); 
            }
          }).then(data => {
                let mydata = data ;
                this.setState({myprojectcriticality : mydata[index]['criticality']});
                this.setState({myprojectisWeb : mydata[index]['isWeb']});
                console.log(mydata[index])
            },)
    }
    /**
     * 
     * @param {*} tab 
     */
    toggleTab(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <Row className="page-title">
                    <Col sm={8} xl={6}>
                        <h4 className="mb-1 mt-0">
                        {this.state.project.title}</h4>
                        
                    </Col>
                    
                </Row>

                <ProjectStats {...this.state.project}/>
                <Row>
                    <Col xl={8}>
                        <About />

                        
                    </Col>
                    <Col xl={4}>
                        <Card>
                            <CardBody>
                                <Details isWeb={this.state.project.isWeb}/>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </React.Fragment >
        );
    }
}

export default ProjectDetail;
