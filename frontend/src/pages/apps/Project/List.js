// @flow
import React, {useEffect, useState} from 'react';
import { Row, Col, Card, CardBody, Progress, UncontrolledTooltip, Button , Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import classNames from 'classnames';
import { Loader } from 'react-feather';

import avatar1 from '../../../assets/images/users/avatar-6.jpg';
import avatar3 from '../../../assets/images/users/avatar-8.jpg';


const renderCards = (todos, len) => {

    const cards =[];
    for(let i = 0; i < len; i++){
        
        cards.push(
        <Col lg={6} xl={4} key={'proj-' + i}>
                <Card>
            <CardBody>
                
                <p className={classNames("text-uppercase", "font-size-12", "mb-2",
                    {
                        'text-success':true
                    })}>{todos[i].domain_name}</p>


                <h5  className="text-dark">
                        {todos[i].scan_name}
                
                </h5>

                <p className="text-muted mb-4">
                    {todos[i].scan_description}...
                </p>
            </CardBody>

            <CardBody className="border-top">
                <Row className="align-items-center">
                    <Col className="col-sm-auto">
                        <ul className="list-inline mb-0">
                            <li className="list-inline-item pr-2">
                                <p className="text-muted d-inline-block" id={`date-${i}`}>
                                    <i className="uil uil-calender mr-1"></i> {todos[i].date}
                                </p>
                                <UncontrolledTooltip placement="top" target={`date-${i}`}>Scan request date</UncontrolledTooltip>
                            </li>
                            
                            <li className="list-inline-item pr-2">
                                {todos[i].WebApp === true &&
                                    (<div className={classNames(
                                    'badge',
                                    {
                                        'badge-success': true
                                    }
                                )}>
                                    Web App
                                </div>)} {' '}
                                {todos[i].DBServer === true &&
                                    (<div className={classNames(
                                    'badge',
                                    {
                                        'badge-info':true    
                                    }
                                )}>
                                    DB Server
                                </div>)}{' '}
                                {todos[i].NetworkServer === true &&
                                    (<div className={classNames(
                                    'badge',
                                    {
                                        'badge-warning':true
                                    }
                                )}>
                                    Network Server
                                </div>)}
                            </li>
                        </ul>
                    </Col>
                    
                </Row>
            </CardBody>
        </Card>
        </Col>
        )
    }
    return cards;
}

const Projects = () => {
    
    const [todos, setTodo] = useState([]);
    const [len, setLen] = useState(0);


    useEffect(() => {
        fetch('http://127.0.0.1:5000/company',{
          method : 'GET'})
        .then(response =>{
            if(response.ok){
              return (response.json())       
            }
            else{
              console.log('some error in getting the list'); 
            }
          }).then((data) =>{ 
                let mylen = data.length;
                setTodo(data);
                setLen(mylen);
            }
          )
        
    },[]);
                

    return (
        <React.Fragment>
            <Row className="page-title">
                <Col md={3} xl={6}>
                    <h4 className="mb-1 mt-0">SCANS</h4>
                </Col>
                <Col md={9} xl={6} className="text-md-right">
                    <div className="mt-4 mt-md-0">
                        <button type="button" className="btn btn-white mr-4 mb-3  mb-sm-0"><i className="uil-plus mr-1"></i><a href='/wizard-form' style={{textColor : 'white'}}>New Scan Request</a></button>
                        <div className="btn-group mb-3 mb-sm-0">
                            <button type="button" className="btn btn-primary">All</button>
                        </div>
                        <div className="btn-group ml-1">
                            <button type="button" className="btn btn-white">Ongoing</button>
                            <button type="button" className="btn btn-white">Finished</button>
                        </div>
                        
                    </div>
                </Col>
            </Row>
            <Row>
                {
                    renderCards(todos,len)
                }
            </Row>

        </React.Fragment>
    );
};

export default Projects;
