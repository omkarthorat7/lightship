import React, {useEffect} from 'react';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

function MyEnv() {
    // form validation rules 
    const validationSchema = Yup.object().shape({
        numberOfTickets: Yup.string()
            .required('Number of environments is required'),
        tickets: Yup.array().of(
            Yup.object().shape({
                name: Yup.string()
                    .required('Name is required'),
                url: Yup.string()
                    .required('URL is required'),
                count: Yup.number()
                    .required('URL is required'),
                
            })
        )
    });

    useEffect(() => {
    	console.log(register);
    	console.log(watch);
    	console.log(errors);
    },[]);
    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, errors, watch } = useForm({
        resolver: yupResolver(validationSchema)
    });

    // watch to enable re-render when ticket number is changed
    const watchNumberOfTickets = watch('numberOfTickets');

    // return array of ticket indexes for rendering dynamic forms in the template
    function ticketNumbers() {
        return [...Array(parseInt(watchNumberOfTickets || 0)).keys()];
    }

    function onSubmit(data) {
        // display form data on success
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
            <div className="card m-3">
                <div className="card-body border-bottom">
                    <div className="form-row">
                        <div className="form-group">
                            <label>Number of Environments</label>
                            <select name="numberOfTickets" ref={register} className={`form-control ${errors?.numberOfTickets ? 'is-invalid' : ''}`}>
                                {['',1,2,3,4,5,6,7,8,9,10].map(i => 
                                    <option key={i} value={i}>{i}</option>
                                )}
                            </select>
                            <div className="invalid-feedback">{errors?.numberOfTickets?.message}</div>
                        </div>
                    </div>
                </div>
                {ticketNumbers().map(i => (
                    <div key={i} className="list-group list-group-flush">
                        <div className="list-group-item">
                            <h5 className="card-title">Environment {i + 1}</h5>
                            <div className="form-row">
                                <div className="form-group col-6">
                                    <label>Name</label>
                                    <input name={`tickets[${i}]name`} ref={register} type="text" className={`form-control ${errors?.tickets?.[i]?.name ? 'is-invalid' : '' }`} />
                                    <div className="invalid-feedback">{errors?.tickets?.[i]?.name?.message}</div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-6">
                                    <label>URL (Target) </label>
                                    <input name={`tickets[${i}]url`} ref={register} type="text" className={`form-control ${errors?.tickets?.[i]?.url ? 'is-invalid' : '' }`} />
                                    <div className="invalid-feedback">{errors?.tickets?.[i]?.url?.message}</div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-6">
                                    <label>Number of Subdomains </label>
                                    <input name={`tickets[${i}]count`} ref={register} type="number" className={`form-control ${errors?.tickets?.[i]?.count ? 'is-invalid' : '' }`} />
                                    <div className="invalid-feedback">{errors?.tickets?.[i]?.count?.message}</div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                ))}
                <div className="card-footer text-center border-top-0">
                    <button type="submit" className="btn btn-primary mr-1">
                        Submit
                    </button>
                    <button className="btn btn-secondary mr-1" type="reset">Reset</button>
                </div>
            </div>
        </form>
    )
}

export { MyEnv };