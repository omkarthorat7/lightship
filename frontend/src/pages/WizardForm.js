import React, { Component } from 'react';

import { Row, Col, Card, CardTitle, CardBody, Form, FormGroup, Label, Input, Button, CustomInput, Alert } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { Checkbox} from "antd";
import { MyEnv } from './myForm';
 
//import PageTitle from '../../components/PageTitle';

class WizardForm extends Component {
    constructor() {
        super();
        this.state = {
            scan_name : '',
            scan_description : '',
            scan_tag : '',
            showTextBox: { status: 0 },
            inputSize: 0,
            showTextArea: { checkedBox: 0 },
            name : '',
            company_name : '',
            contact_no : '',
            email : '',
            isemailvalid : true,
            domain_name : '',
            envNum : '',
            envNames : [],
            envUrls : [],
            envSubdomains : [],
            subdomainName : [],
            InScope : [],
            API_endpoints :'',
            DBServer : false,
            WebApp :false,
            NetworkServer : false,
            date : '',
            OutOfScopevul : '',
            NotesFromClient : '',
            username1 : '',
            username2 : '',
            password1 : '',
            password2 : '',
            submitted : false,
        };
        this.NameChange = this.NameChange.bind(this);
        this.CompanyChange = this.CompanyChange.bind(this); 
        this.ContactChange = this.ContactChange.bind(this); 
        this.EmailChange = this.EmailChange.bind(this);
        this.domainChange = this.domainChange.bind(this); 
        this.APIChange = this.APIChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
        this.renderInputs = this.renderInputs.bind(this);
        this.envnamechange = this.envnamechange.bind(this);
        this.envurlchange = this.envurlchange.bind(this);
        this.envsubdomainchange = this.envsubdomainchange.bind(this);
        this.subdomainNamechange = this.subdomainNamechange.bind(this);
        this.inscopeHandler = this.inscopeHandler.bind(this);
    }


    handleOnChange(value) {
        this.setState({
            inputSize: value.target.value,
        });
    }
    envnamechange(e, i){
        console.log(e.target.value);
        let enames = [...this.state.envNames];
        let ename = {...enames[i]};
        ename = e.target.value;
        enames[i] = ename;
        this.setState({
            envNames : enames,
        })
    }
    envurlchange(e, i){
        console.log(e.target.value);
        let eurls = [...this.state.envUrls];
        let eurl = {...eurls[i]};
        eurl = e.target.value;
        eurls[i] = eurl;
        this.setState({
            envUrls : eurls,
        })
    }
    envsubdomainchange(e, i){
        console.log(e.target.value);
        let esubdomians = [...this.state.envSubdomains];
        let esubdomian = {...esubdomians[i]};
        esubdomian = e.target.value;
        esubdomians[i] = esubdomian;
        this.setState({
            envSubdomains : esubdomians,
        })
    }
    subdomainNamechange(e, index, i){
        console.log(e.target.value);
        let listoflistofnames = [...this.state.subdomainName];
        let subdomainNames = {...listoflistofnames[index]};
        let subdomainname = {...subdomainNames[i]};
        subdomainname = e.target.value;
        subdomainNames[i] =subdomainname;
        listoflistofnames[index] = subdomainNames;
        this.setState({
            subdomainName : listoflistofnames,
        })

    }

    inscopeHandler(val, index, i){
        let listoflistofInscopes = [...this.state.InScope];
        let inscopes = {...listoflistofInscopes[index]};
        let inscope = {...inscopes[i]};
        inscope = val===1?true:false;
        inscopes[i] =inscope;
        listoflistofInscopes[index] = inscopes;
        this.setState({
            InScope : listoflistofInscopes,
        })
    }

    renderEnvironments(value){
        const env = [];
        const inputs =[];
        
        for(let i = 0; i < value; i++){
            env.push(
                <Card>
                <CardBody>
                <CardTitle tag="h5">Environment {i+1}</CardTitle>
                <Form >
                    <FormGroup row>
                        <Label for="phone" md={3}>
                            Name
                        </Label>
                        <Col md={9}>
                            <Input
                                type="text"
                                name="name"
                                placeholder="Enter name"
                                value = {this.state.envNames[i]}
                                onChange = {(e) => {this.envnamechange(e,i)}}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="phone" md={3}>
                            URL (Target)
                        </Label>
                        <Col md={9}>
                            <Input
                                type="text"
                                name="url"
                                placeholder="Enter url"
                                value={this.state.envUrls[i]}
                                onChange ={(e)=>{this.envurlchange(e, i)}}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="phone" md={3}>
                            Number of Subdomains
                        </Label>
                        <Col md={9}>
                            <Input
                                type="number"
                                name="domains"
                                placeholder="Enter number of domains"
                                value ={this.state.envSubdomains[i]}
                                onChange={(e)=>{this.envsubdomainchange(e, i)}}
                            />
                            {this.renderInputs(this.state.envSubdomains[i], i)}
                        </Col>
                    </FormGroup>
                </Form>
                </CardBody>
                </Card>
            )
        }
        return env;
    }

    renderInputs(value, index) {
        const inputs = [];
        let val = true;
        for (let i = 0; i < value; i++) {
            inputs.push(
                <Card><Form className="mt-3">
                    <FormGroup row>
                        <Label for="exampleEmail" md={2}>
                            Subdomain
                        </Label>
                        <Col>
                            <Input type="text" name="name" 
                            id="exampleEmail" placeholder="Enter subdomain " 
                            value={this.state.subdomainName[index] ? this.state.subdomainName[index][i] : ''} 
                            onChange={(e)=>{this.subdomainNamechange(e,index, i)}}/>
                        </Col>
                    </FormGroup>
                    <FormGroup>
                        <div className="d-flex">
                            <Col md={7}><FormGroup check>
                              <Label check>
                                <input
                                type="radio"
                                name="release"
                                checked={this.state.InScope[index] ? this.state.InScope[index][i] : false}
                                onChange={(e) => this.inscopeHandler(1, index, i)}
                                />{' '}INSCOPE
                              </Label>
                            </FormGroup></Col>
                            <Col md={7}><FormGroup check>
                              <Label check>
                                <input
                                type="radio"
                                name="release"
                                checked={this.state.InScope[index] ? !this.state.InScope[index][i] : true} 
                                onChange={(e) => this.inscopeHandler(2, index, i)}
                                />{' '}OUTSCOPE
                              </Label>
                            </FormGroup></Col>
                        </div>
                    </FormGroup>
                </Form></Card>
            );
        }
        return inputs;
    }

    radioHandler = (status) => {
        this.setState({ status });
        console.log(this.state);
    };

    changeHandler = (checkedBox) => {
        this.setState({ checkedBox });
    };

    NameChange(event) {
    //handle the changes in name and update them in state
      this.setState({name: event.target.value});
      console.log(event.target.value);
    }

    CompanyChange(event) {
    //handle the changes in company name and update them in state
      this.setState({company_name : event.target.value});
      console.log(event.target.value);
    }

    ContactChange(event) {
    //handle the changes in contact number and update them in state
      this.setState({contact_no : event.target.value});
      console.log(event.target.value);
    }

    EmailChange(event) {
        //handle the changes in email and update them in state
      this.setState({email : event.target.value});
      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ( re.test(this.state.email) || this.state.email === '') {
            this.setState({isemailvalid : true})
        }
        else {
            this.setState({isemailvalid : false})
        }
      console.log(event.target.value);
    }

    domainChange(event) {
        //handle the changes in the domain name and update them in the state
      this.setState({domain_name : event.target.value});
      console.log(event.target.value);
    }

    APIChange(event){
        //handle the changes in the API endpoints and update them in the state
      this.setState({API_endpoints : event.target.value});
      console.log(event.target.value);
    }

    formSubmit(event){
        //handle the submission of the form
        this.setState({submitted : true});
        console.log(this.state);
        console.log('sending post request');
        event.preventDefault();
        fetch('http://localhost:5000/company/create',{
          method : 'POST', 
          headers: { 'Content-Type': 'application/json' },
          body : JSON.stringify({responsedata : this.state})}).then(response =>{
            return response.json()
        }).then(data => console.log(data))
    }

    render() {
        const { status, checkedBox, name, company_name, contact_no, email, isemailvalid } = this.state;
        const enabled1 = name.length > 0 && company_name.length > 0 && contact_no.length > 0 && email.length > 0 && isemailvalid;
        const inputs = [];

        for (let i = 1; i <= this.state.total; i++) {
            inputs.push(<input name={`input-${i}`} onChange={this.onChange} />);
        }

        return (
            <Card className="mt-4">
                <CardBody>
                    <h4 className="header-title mt-0 mb-1">Scan Request</h4>

                    <Wizard>
                        <Steps>
                            <Step
                                id="login"
                                render={({ next }) => (
                                    <Form>
                                        <Card><CardBody><FormGroup row>
                                            <Label md={3}>
                                                Scan Name
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="name"
                                                    id="exampleEmail"
                                                    placeholder="Enter scan name"
                                                    value={this.state.scan_name}
                                                    onChange={(e)=>this.setState({scan_name : e.target.value})}
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup row>
                                            <Label md={3}>
                                                Scan Description
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="name"
                                                    id="exampleEmail"
                                                    placeholder="Enter scan description"
                                                    value={this.state.scan_description}
                                                    onChange={(e)=>this.setState({scan_description : e.target.value})}
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup row>
                                            <Label md={3}>
                                                Tags
                                            </Label>
                                            <Col md={9}>
                                                <Input type="select" name="select" 
                                                    value={this.state.scan_tag}
                                                    onChange={(e)=>this.setState({scan_tag : e.target.value})}
                                                >
                                                  <option>Tag 1</option>
                                                  <option>Tag 2</option>
                                                  <option>Tag 3</option>
                                                  <option>Tag 4</option>
                                                  <option>Tag 5</option>
                                                </Input>
                                            </Col>
                                        </FormGroup>
                                        </CardBody></Card>
                                        <FormGroup row>
                                            <Label for="exampleEmail" md={3}>
                                                Name
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="name"
                                                    id="exampleEmail"
                                                    placeholder="Enter name"
                                                    value = {this.state.name}
                                                    onChange = {this.NameChange}
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Label for="examplePassword" md={3}>
                                                Company
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="company"
                                                    id="examplePassword"
                                                    placeholder="Enter company"
                                                    value = {this.state.company_name}
                                                    onChange = {this.CompanyChange}
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Label for="examplerePassword" md={3}>
                                                Contact No.
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="number"
                                                    name="contact"
                                                    id="examplerePassword"
                                                    placeholder="Enter contact no."
                                                    value = {this.state.contact_no}
                                                    onChange= {this.ContactChange}
                                                    
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Label for="examplerePassword" md={3}>
                                                Email
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="email"
                                                    name="email"
                                                    id="email"
                                                    placeholder="xyz@xyz.com"
                                                    value = {this.state.email}
                                                    onChange= {this.EmailChange}
                                                    
                                                />
                                            </Col>
                                        </FormGroup>
                                        <Alert color="light" isOpen={!this.state.isemailvalid} >
                                          Please ensure that the email entered is valid!
                                        </Alert>
                                        <ul className="list-inline wizard mb-0">
                                            <li className="next list-inline-item float-right">
                                                <Button disabled={!enabled1} onClick={next} color="success">
                                                    Next
                                                </Button>
                                            </li>
                                        </ul>
                                    </Form>
                                )}
                            />
                            <Step
                                id="gandalf"
                                render={({ next, previous }) => (
                                    <Form>
                                        <FormGroup row>
                                            <Label for="fname" md={3}>
                                                Domain Name
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="fname"
                                                    id="fname"
                                                    placeholder="Enter your domain name"
                                                    value={this.state.domain_name}
                                                    onChange={this.domainChange}
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Label for="lname" md={3}>
                                                Do you have multiple enviornments
                                            </Label>
                                            <div className="d-flex mt-2 ml-3">
                                                <div className="mr-4">
                                                    <input
                                                        type="radio"
                                                        name="release"
                                                        checked={status === 1}
                                                        onClick={(e) => this.radioHandler(1)}
                                                    />
                                                    <Label className="ml-2">Yes</Label>
                                                </div>
                                                <div>
                                                    <input
                                                        type="radio"
                                                        name="release"
                                                        checked={status === 2}
                                                        onClick={(e) => this.radioHandler(2)}
                                                    />
                                                    <Label className="ml-2">No</Label>
                                                </div>
                                            </div>
                                        </FormGroup>
                                        {status === 1 && (
                                            <Form>
                                            <FormGroup row>
                                                <Label for="phone" md={3}>
                                                    Enter the number of enviornments
                                                </Label>
                                                <Col md={9}>
                                                    <Input
                                                        type="number"
                                                        name="enviornments"
                                                        id="phone"
                                                        placeholder="Enter number of enviornments"
                                                        value={this.state.envNum}
                                                        onChange={(e)=> this.setState({envNum: e.target.value})}
                                                    />
                                                    
                                                </Col>
                                            </FormGroup>
                                            </Form>
                                        )}
                                        {status === 1 
                                        && (this.renderEnvironments(this.state.envNum))
                                           }

                                        <FormGroup row>
                                            <Label for="phone" md={3}>
                                                What API endpoints do you want to outscope
                                            </Label>
                                            <Col md={9}>
                                                <Input
                                                    type="text"
                                                    name="phone"
                                                    id="phone"
                                                    placeholder="Enter api endpoints"
                                                    value={this.state.API_endpoints}
                                                    onChange={this.APIChange}
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Label for="lname" md={3}>
                                                What systems are inscope for testing
                                            </Label>
                                            <div className="mr-5 mt-2">
                                                <Checkbox
                                                    checked={this.state.WebApp}
                                                    onChange ={(e) => this.setState({ WebApp: e.target.checked})}
                                                > Web App </Checkbox>
                                            </div>
                                            <div className=" mr-5 mt-2">
                                                <Checkbox
                                                    checked={this.state.DBServer}
                                                    onChange ={(e) => this.setState({ DBServer: e.target.checked})}
                                                > DB Server </Checkbox>
                                            </div>
                                            <div className=" mt-2">
                                                <Checkbox
                                                    checked={this.state.NetworkServer}
                                                    onChange ={(e) => this.setState({ NetworkServer: e.target.checked})}
                                                > Network Server </Checkbox>
                                            </div>
                                        </FormGroup>

                                        <ul className="list-inline wizard mb-0">
                                            <li className="previous list-inline-item">
                                                <Button onClick={previous} color="info">
                                                    Previous
                                                </Button>
                                            </li>
                                            <li className="next list-inline-item float-right">
                                                <Button onClick={next} color="success">
                                                    Next
                                                </Button>
                                            </li>
                                        </ul>
                                    </Form>
                                )}
                            />
                            <Step
                                id="dumbledore"
                                render={({ previous }) => (
                                    <div>
                                        <Form>
                                            <FormGroup row>
                                                <Label for="lname" md={3}>
                                                    Have you done any such testing before
                                                </Label>
                                                <div className="d-flex ml-3 mt-2">
                                                    <div className="mr-4">
                                                        <input
                                                            type="radio"
                                                            name="release"
                                                            checked={checkedBox === 1}
                                                            onClick={(e) => this.changeHandler(1)}
                                                        />
                                                        <Label className="ml-2">Yes</Label>
                                                    </div>
                                                    <div>
                                                        <input
                                                            type="radio"
                                                            name="release"
                                                            checked={checkedBox === 2}
                                                            onClick={(e) => this.changeHandler(2)}
                                                        />
                                                        <Label className="ml-2">No</Label>
                                                    </div>
                                                </div>
                                            </FormGroup>
                                            {checkedBox === 1 && (
                                                <FormGroup row>
                                                    <Label for="phone" md={3}>
                                                        Do you want to keep certain vulnerabilities out of scope
                                                    </Label>
                                                    <Col md={9}>
                                                        <Input type="textarea" name="phone" id="phone" 
                                                        rows={3} 
                                                        value={this.state.OutOfScopevul}
                                                        onChange={(e)=> this.setState({OutOfScopevul: e.target.value})}
                                                        />
                                                    </Col>
                                                </FormGroup>
                                            )}
                                            <FormGroup row>
                                                <Label for="phone" md={3}>
                                                    When will you like us to start with the testing
                                                </Label>
                                                <Col md={4}>
                                                    <Input
                                                        type="date"
                                                        name="phone"
                                                        id="phone"
                                                        value={this.state.date}
                                                        onChange={(e)=>this.setState({date : e.target.value})}
                                                    />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="phone" md={3}>
                                                    Are there any security clearance forms that we need to be aware of -
                                                    we will sign them and send it to you
                                                </Label>
                                                <Col md={9}>
                                                    <Input type="file" name="phone" id="phone" />
                                                </Col>
                                            </FormGroup>
                                            <div className="mt-4">
                                                <Label>Attached Files</Label>

                                                <div className="row mb-3">
                                                    <div className="col-xl-4 col-md-6">
                                                        <div className="p-2 border rounded mb-2">
                                                            <div className="media">
                                                                <div className="avatar-sm font-weight-bold mr-3">
                                                                    <span className="avatar-title rounded bg-soft-primary text-primary">
                                                                        <i className="uil-file-plus-alt font-size-18"></i>
                                                                    </span>
                                                                </div>
                                                                <div className="media-body">
                                                                    <a href="/" className="d-inline-block mt-2">
                                                                        Landing 1.psd
                                                                    </a>
                                                                </div>
                                                                <div className="float-right mt-1">
                                                                    <a href="/" className="p-2">
                                                                        <i className="uil-download-alt font-size-18"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-xl-4 col-md-6">
                                                        <div className="p-2 border rounded mb-2">
                                                            <div className="media">
                                                                <div className="avatar-sm font-weight-bold mr-3">
                                                                    <span className="avatar-title rounded bg-soft-primary text-primary">
                                                                        <i className="uil-file-plus-alt font-size-18"></i>
                                                                    </span>
                                                                </div>
                                                                <div className="media-body">
                                                                    <a href="/" className="d-inline-block mt-2">
                                                                        Landing 2.psd
                                                                    </a>
                                                                </div>
                                                                <div className="float-right mt-1">
                                                                    <a href="/" className="p-2">
                                                                        <i className="uil-download-alt font-size-18"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <FormGroup row>
                                                <Label for="phone" md={3}>
                                                    Is there anything else you would like us to know ? 
                                                </Label>
                                                <Col md={9}>
                                                    <Input type="textarea" rows="3" 
                                                    value={this.state.NotesFromClient}
                                                    onChange={(e)=> this.setState({NotesFromClient: e.target.value})}
                                                    />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup row>
                                                <Label for="phone" md={3}>
                                                    For testing we require two sets of credentials. Please provide it
                                                    from your side if its not a public signup platform
                                                </Label>
                                                <Row className="ml-0">
                                                    <Col md={6}>
                                                        <Input
                                                            type="text"
                                                            className="mb-3"
                                                            placeholder="Enter Username"
                                                            value={this.state.username1}
                                                            onChange={(e)=> this.setState({username1: e.target.value})}
                                                        />
                                                    </Col>
                                                    <Col md={6}>
                                                        <Input
                                                            type="password"
                                                            className="mb-3"
                                                            placeholder="Enter Password"
                                                            value={this.state.password1}
                                                            onChange={(e)=>this.setState({password1: e.target.value})}
                                                        />
                                                    </Col>
                                                    <Col md={6}>
                                                        <Input type="text" placeholder="Enter Username" 
                                                            value={this.state.username2}
                                                            onChange={(e)=>this.setState({username2: e.target.value})}
                                                        />
                                                    </Col>
                                                    <Col md={6}>
                                                        <Input type="password" placeholder="Enter Password" 
                                                            value={this.state.password2}
                                                            onChange={(e)=>this.setState({password2: e.target.value})}
                                                        />
                                                    </Col>
                                                </Row>
                                            </FormGroup>
                                        </Form>

                                        <Row>
                                            <Col sm={12}>
                                                <ul className="list-inline wizard mb-0">
                                                    <li className="previous list-inline-item">
                                                        <Button onClick={previous} color="info">
                                                            Previous
                                                        </Button>
                                                    </li>

                                                    <li className="next list-inline-item float-right">
                                                        <Button color="success" onClick={(e) => {this.formSubmit(e)}}>Submit</Button>
                                                    </li>
                                                </ul>
                                            </Col>
                                        </Row>
                                        {this.state.submitted === true && (
                                        <div><br></br>
                                        <Row><Card style={{width : 'null', height : 'null', flex : 1}}><CardBody>
                                        <h2 style={{textAlign : 'center'}}>THANK YOU</h2>
                                        <p style={{textAlign : 'center'}}>The scan request has been submitted</p>
                                        </CardBody></Card>
                                        </Row></div>)
                                        }
                                    </div>
                                )}
                            />

                        </Steps>
                    </Wizard>
                </CardBody>
            </Card>
        );
    }
}

export default WizardForm;
