import React, { Component , useEffect, useState} from 'react';
import { Row, Card, Col, CardBody, CardTitle, Table , Button } from 'reactstrap';
import "./styles.css";

import ProjectDetail from '../apps/Project/Detail/index.js';
// import Flatpickr from 'react-flatpickr';
// import { ChevronDown, Mail, Printer, File, Users, Image, ShoppingBag } from 'react-feather';
import Chart from 'react-apexcharts';
// import { getLoggedInUser } from '../../helpers/authUtils';
// import Loader from '../../components/Loader';
// import OverviewWidget from '../../components/OverviewWidget';
import { Media } from 'reactstrap';
import classNames from 'classnames';
// import Statistics from './Statistics';
// import RevenueChart from './RevenueChart';
// import TargetChart from './TargetChart';
// import SalesChart from './SalesChart';
// import Orders from './Orders';
// import Performers from './Performers';
// import Tasks from './Tasks';
// import Chat from './Chat';

const Activity = (props) => {
    return (
        <Media>
            <div className="text-center mr-3">
                <div className="avatar-sm">
                    <span className={classNames('avatar-title', 'rounded-circle', props.classNames)}>{props.day}</span>
                </div>
                <p className="text-muted font-size-13 mb-0">{props.month}</p>
            </div>
            <div className="media-body overflow-hidden">
                <h5 className="font-size-15 mt-2 mb-1">
                    <a href="/" className="text-dark">
                        {props.user}
                    </a>
                </h5>
                <p className="text-muted font-size-13 text-truncate mb-0">{props.description}</p>
            </div>
        </Media>
    );
};

const options = {
    chart: {
        height: 302,
        type: 'donut',
        // toolbar: {
        //     show: false,
        // },
        parentHeightOffset: 0,
    },
    //colors: ['#5369f8', '#43d39e', '#f77e53', '#ffbe0b'],
    // grid: {
    //     borderColor: '#f1f3fa',
    //     padding: {
    //         left: 0,
    //         right: 0,
    //     },
    // },
    // plotOptions: {
    //     pie: {
    //         donut: {
    //             size: '70%',
    //         },
    //         expandOnClick: false,
    //     },
    // },
    // legend: {
    //     show: true,
    //     position: 'right',
    //     horizontalAlign: 'left',
    //     itemMargin: {
    //         horizontal: 6,
    //         vertical: 3,
    //     },
    // },
    // labels: ['Clothes 44k', 'Smartphons 55k', 'Electronics 41k', 'Other 17k'],
    // responsive: [
    //     {
    //         breakpoint: 480,
    //         options: {
    //             legend: {
    //                 position: 'bottom',
    //             },
    //         },
    //     },
    // ],
    // tooltip: {
    //     y: {
    //         formatter: function (value) {
    //             return value + 'k';
    //         },
    //     },
    // },
};

const data = [44, 55, 41, 17];
const Vulnerabilities_cols = ["Severity", "Target", "Name", "Owner", "Date", "Details"];
const table_entries = [{Severity : "CRITICAL", Target : "192.168.20.41", Name : "SQL Injection - PreAuth", Owner : "Omkar", Date: "a year ago"},
                    {Severity : "INFO", Target : "192.168.20.38", Name : "Traceroute Information", Owner : "Omkar", Date: "a month ago"},
                    {Severity : "INFO", Target : "192.168.20.41", Name : "Nessus SYN Scanner", Owner : "Bob", Date: "a year ago"},
                    {Severity : "INFO", Target : "192.168.20.41", Name : "Nessus SYN Scanner", Owner : "Abhishek", Date: "a month ago"}];


function Dashboard() {
    useEffect(() => {
        fetch('http://127.0.0.1:4000/vulnerability',{
          method : 'GET'})
        .then(response =>{
            if(response.ok){
              return (response.json())       
            }
            else{
              console.log('some error in getting the list'); 
            }
          }).then(data => console.log(data))
        
    },[]);

     
    return (
        <React.Fragment>
            <div className="">
                {/* preloader */}

                <Row className="page-title align-items-center"></Row>
                <div>
                    <Row>
                        <Col lg="6"></Col>
                    </Row>
                </div>
                <Row>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-primary">9</h3>
                                    <label className="text-primary">CRITICAL</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-danger">9</h3>
                                    <label className="text-danger">HIGH</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-warning">24</h3>
                                    <label className="text-warning">MED</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-success">11</h3>
                                    <label className="text-success">LOW</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-info">248</h3>
                                    <label className="text-info">INFO</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="2">
                        <Card>
                            <CardBody>
                                <div className="text-center">
                                    <h3 className="text-primary">8</h3>
                                    <label className="text-primary">UNCLASSIFIED</label>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col lg="4">
                        <Card>
                            <CardBody className="">
                                <h5 className="card-title mt-0 mb-0 header-title">Vulnerabilities</h5>

                                <Chart
                                    options={options}
                                    series={data}
                                    type="donut"
                                    className="apex-charts mb-0 mt-4"
                                    height={302}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="4">
                        <Card>
                            <CardBody className="">
                                <h5 className="card-title mt-0 mb-0 header-title">Vulnerabilities by Status</h5>

                                <Chart
                                    options={options}
                                    series={data}
                                    type="donut"
                                    className="apex-charts mb-0 mt-4"
                                    height={302}
                                />
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="4">
                        <Card>
                            <CardBody>
                                <div className="d-flex justify-content-between">
                                    <h6 className="mt-0 header-title">Activity Feed</h6>
                                    <label className="text-primary">View More</label>
                                </div>
                                <ul className="list-unstyled activity-widget">
                                    <li className="activity-list">
                                        <Activity
                                            day="09"
                                            month="Jan"
                                            user="Bob"
                                            description={`import Nessus report and found: 55 vulnerabilities`}
                                            classNames="bg-soft-primary text-primary"
                                        />
                                    </li>
                                    <li className="activity-list">
                                        <Activity
                                            day="08"
                                            month="Jan"
                                            user="Susan"
                                            description="ran nmap and found: 1 service"
                                            classNames="bg-soft-success text-success"
                                        />
                                    </li>
                                    <li className="activity-list">
                                        <Activity
                                            day="08"
                                            month="Jan"
                                            user="Susan"
                                            description="ran python and found: 5 vulnerabilities"
                                            classNames="bg-soft-warning text-warning"
                                        />
                                    </li>
                                </ul>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col lg="6">
                        <Card>
                            <CardTitle className="ml-3 mt-4">
                                {' '}
                                <h6>STATUS REPORT</h6>
                            </CardTitle>
                            <CardBody>
                                <Row>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">9</h4>
                                            <label>NEW</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">4</h4>
                                            <label>APPROVED</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">5</h4>
                                            <label>IN-PROGRESS</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">5</h4>
                                            <label>RESOLVED</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">4</h4>
                                            <label>FIXED</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-primary">
                                            <h4 className="text-primary">3</h4>
                                            <label>IRRELEVANT</label>
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg="6">
                        <Card>
                            <CardTitle className="ml-3 mt-4">
                                <h6>WORKSPACE SUMMARIZED REPORT</h6>
                            </CardTitle>
                            <CardBody>
                                <Row>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">7</h4>
                                            <label>HOSTS</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">2</h4>
                                            <label>CREDENTIALS</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">77</h4>
                                            <label>SERVICES</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">270</h4>
                                            <label>VULNS</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">309</h4>
                                            <label>TOTAL VULNS</label>
                                        </div>
                                    </Col>
                                    <Col lg="4">
                                        <div className="text-center text-info">
                                            <h4 className="text-info">39</h4>
                                            <label>WEB VULNS</label>
                                        </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col lg="12">
                        <Card>
                            <CardBody className="pb-0">
                                <h5 className="card-title mt-0 mb-0 header-title">Vulnerabilities</h5>

                                <Table hover responsive className="mt-4">
                                    <thead>
                                        <tr>
                                            {Vulnerabilities_cols.map((object, index) => {
                                                return (<th scope="col" key={`${object}_${index}`}>{object}</th>)
                                            })}

                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        {table_entries.map((object, index) => {
                                            return (
                                               
                                                <tr key={`${object}_${index}`}>
                                                    <td>
                                                        <span className="badge badge-soft-danger py-1">{object.Severity}</span>
                                                    </td>
                                                    <td>{object.Target}</td>
                                                    <td>{object.Name}</td>
                                                    <td>{object.Owner}</td>
                                                    <td>{object.Date}</td>
                                                    <td><Button color="primary" onClick={event => window.location.href=`/apps/projects/detail`}>View Details</Button>
                                                      
                                                    </td>
                                                </tr>
                                                
                                            )
                                        })}
                                        
                                    </tbody>
                                    
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                
            </div>
        </React.Fragment>
    );
}

export default Dashboard;
